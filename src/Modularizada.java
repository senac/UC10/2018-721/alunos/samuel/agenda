
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class Modularizada {

    public static void showMessageInfo(String m) {
        JOptionPane.showMessageDialog(null, m, "Informação", JOptionPane.INFORMATION_MESSAGE);

    }

    public static void showMessageWarnning(String m) {
        JOptionPane.showMessageDialog(null, m, "Advertencia", JOptionPane.WARNING_MESSAGE);

    }

    public static void showMessageErro(String m) {
        JOptionPane.showMessageDialog(null, m, "ERRO", JOptionPane.ERROR_MESSAGE);

    }

    public static Contatos pesquisaContato(List<Contatos> lista, String nome) {
        if (!lista.isEmpty()) {
            for (Contatos c : lista) {

                if (c.nome.equals(nome)) {
                    return c;

                }

            }

        }

        return null;
    }

    public static void main(String[] args) {
        List<Contatos> contato = new ArrayList<>();
        String c;
        int op;
        Contatos contatos = null;
        String nome;
        String telefone;
        for (;;) {
            boolean achei = false;

            String menu = String.format("########### Agenda de Contatos ###########\n"
                    + "                                Contatos(%d)\n"
                    + "1 - Novo Contato\n"
                    + "2 - Buscar Contato por Nome\n"
                    + "3 - Remover Contato\n"
                    + "4 - Lista Todos Contatos\n"
                    + "5 - Sobre Agenda de Contatos\n"
                    + "0 - Sair", contato.size());

            c = JOptionPane.showInputDialog(menu);
            op = Integer.parseInt(c);

            switch (op) {
                case 1:
                    nome = JOptionPane.showInputDialog("Nome");

                    for (int i = 0; i < contato.size(); i++) {
                        if (nome.equalsIgnoreCase(contato.get(i).nome)) {
                            achei = true;
                        }
                    }

                    if (!achei) {
                        telefone = JOptionPane.showInputDialog("Telefone");
                        contatos = new Contatos();
                        contatos.nome = nome;
                        contatos.telefone = telefone;
                        

                        contato.add(contatos);
                        JOptionPane.showMessageDialog(null, "Contato Salvo com Sucesso!!");
                    } else {
                        JOptionPane.showMessageDialog(null, "Contato Já Existente!!");
                    }

                    break;

                case 2:
                    if (!contato.isEmpty()) {
                        nome = JOptionPane.showInputDialog("Digite O Nome Que Vai Ser Buscado");
                        for (int i = 0; i < contato.size(); i++) {
                            contatos = contato.get(i);
                            if (contatos.nome.equals(nome)) {
                                achei = true;
                            }
                        }

                        if (achei) {
                            JOptionPane.showMessageDialog(null, contatos.nome + "\n" + contatos.telefone);
                        } else {
                            JOptionPane.showMessageDialog(null, "O Nome Desejado Não Foi Encontrado");
                        }

                    } else {
                        JOptionPane.showMessageDialog(null, "A Lista Está Vazia!");
                    }

                    break;

                case 3:
                    if (!contato.isEmpty()) {
                        int posicao = 0;
                        nome = JOptionPane.showInputDialog("Digite o Nome Que Queira Remover");
                        for (int i = 0; i < contato.size(); i++) {
                            contatos = contato.get(i);
                            if (contatos.nome.equals(i)) {
                                achei = true;
                                posicao = i;
                            }

                        }

                        if (achei) {
                            contato.remove(posicao);
                            JOptionPane.showMessageDialog(null, "Contato Removido Com Sucesso!!");
                        } else {
                            JOptionPane.showMessageDialog(null, "Nome Procurado Não Forá Encontrado");
                        }

                    } else {
                        JOptionPane.showMessageDialog(null, "A Lista Está Vazia");
                    }

                    break;

                case 4:
                    String listaDeContatos = new String();

                    for (int i = 0; i < contato.size(); i++) {
                        listaDeContatos += contato.get(i).nome + " - " + contato.get(i).telefone + "\n";

                    }
                    JOptionPane.showMessageDialog(null, listaDeContatos);
                    break;

                case 5:
                    String sobre = "Desenvolvida pelo : Samuel Batista Ribeiro \n"
                            + "Turma: 721 De 2018 ";
                    JOptionPane.showMessageDialog(null, sobre);

                    break;

                case 0:
                    System.exit(0);

            }
        }
    }
   
}
